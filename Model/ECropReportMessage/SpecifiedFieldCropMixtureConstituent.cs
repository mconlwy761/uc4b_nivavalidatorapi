﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// A specified field crop mixture constituent.
	/// </summary>
	public partial class SpecifiedFieldCropMixtureConstituent
	{
		/// <summary>
		/// This constituent percentwise part of the whole mix.
		/// </summary>
		[JsonProperty("CropProportionPercent", Required = Required.Always)]
		public double CropProportionPercent { get; set; }

		/// <summary>
		/// Plants or produce cultivated from a single botanical species or variety.
		/// </summary>
		[JsonProperty("SpecifiedBotanicalCrop", Required = Required.Always)]
		public SpecifiedBotanicalCrop SpecifiedBotanicalCrop { get; set; }
	}
}