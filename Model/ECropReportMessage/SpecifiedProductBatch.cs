﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
    /// <summary>
    /// A group of products considered or dealt with together.
    /// </summary>
    public partial class SpecifiedProductBatch
    {
        /// <summary>
        /// A treatment, described as text, applied to this product batch.
        /// </summary>
        [JsonProperty("AppliedTreatment", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string AppliedTreatment { get; set; }

        /// <summary>
        /// The identifier for this product batch.
        /// </summary>
        [JsonProperty("ID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// The product name, expressed as text, for this product batch.
        /// </summary>
        [JsonProperty("ProductName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string ProductName { get; set; }

        /// <summary>
        /// The size, expressed as cubic metres, for one unit in this product batch.
        /// </summary>
        [JsonProperty("SizeMeasure", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public double? SizeMeasure { get; set; }

        /// <summary>
        /// The code specifying the type of product batch.
        /// </summary>
        [JsonProperty("TypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string TypeCode { get; set; }

        /// <summary>
        /// The number of units used for this product batch.
        /// </summary>
        [JsonProperty("UnitQuantity", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// The weight, expressed as kg, for one unit in this product batch.
        /// </summary>
        [JsonProperty("WeightMeasure", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public double? WeightMeasure { get; set; }
    }
}