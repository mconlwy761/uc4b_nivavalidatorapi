﻿using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class PhysicalSpecifiedGeographicalFeatureValidator : AbstractValidator<PhysicalSpecifiedGeographicalFeature>
	{
		public PhysicalSpecifiedGeographicalFeatureValidator()
		{
			RuleFor(x => x.Geometry)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new GeometryValidator());
			RuleFor(x => x.Type)
				.SetValidator(new PhysicalSpecifiedGeographicalFeatureTypeValidator());
		}
	}
}
