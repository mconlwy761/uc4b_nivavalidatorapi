﻿using System;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class DateTimeValidator : AbstractValidator<string>
	{
		public DateTimeValidator()
		{
			RuleFor(x => x)
				.Must(x => DateTime.TryParse(x, out _) == true)
				.WithState(x => MessageCode.DateTimeFormatNotValid);
		}
	}
}