﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedProductBatchValidator : AbstractValidator<SpecifiedProductBatch>
	{
		/*
		 * 1195068 NS 26-15
		 * 1195256 NPKS 22- 2-13- 3
		 * 1682707 Kalkammonsalpeter 27 N (PROD, centernormsæt SEGES English norms, used for actual NL farm demo Niva Durch Farm) (ed. 28-05-2020
		 * 1682276 Kalkammonsalpeter 27 N (DEV, centernormsæt SEGES English norms, used for internal testfarm Niva Project Testfarm) (ed. 28-05-2020)
		 * 1195069 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 * 1671574 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 * 1195041 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 *
	    */
		static string[] ValidCommercialFertilizerIds = new string[] {"1195068", "1195256", "1682707", "1682276", "1195069", "1671574", "1195041"};

		public SpecifiedProductBatchValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			if (reportTypeCode == ReportTypeCode.SMN)
			{
				RuleFor(x => x.Id)
					.NotNull()
					.WithState(x1 => MessageCode.NullField)
					.Must(x2 => ValidCommercialFertilizerIds.Contains(x2))
					.WithState(x3 => MessageCode.SpecifiedProductBatchIdNotSupported);
			}
		}
	}
}
