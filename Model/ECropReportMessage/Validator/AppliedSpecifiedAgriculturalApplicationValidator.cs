﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class AppliedSpecifiedAgriculturalApplicationValidator :AbstractValidator<AppliedSpecifiedAgriculturalApplication>
	{
		public AppliedSpecifiedAgriculturalApplicationValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.AppliedSpecifiedAgriculturalApplicationRate)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new AppliedAgriculturalApplicationRateValidator()));
			RuleFor(x => x.SpecifiedProductBatch)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new SpecifiedProductBatchValidator(organisationCode, reportTypeCode)));
		}
	}
}
