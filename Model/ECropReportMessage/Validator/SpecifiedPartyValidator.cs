﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedPartyValidator : AbstractValidator<SpecifiedParty>
	{
		public SpecifiedPartyValidator()
		{
			RuleFor(x => x.Id)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.CountryId)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.Name)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
		}
	}
}
