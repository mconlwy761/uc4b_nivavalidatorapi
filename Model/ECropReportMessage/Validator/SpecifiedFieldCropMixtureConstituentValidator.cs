﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedFieldCropMixtureConstituentValidator :AbstractValidator<SpecifiedFieldCropMixtureConstituent>
	{
		public SpecifiedFieldCropMixtureConstituentValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.SpecifiedBotanicalCrop)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedBotanicalCropValidator(organisationCode));

		}
	}
}
