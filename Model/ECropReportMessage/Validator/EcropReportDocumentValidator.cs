﻿using System.Collections.Generic;
using System.Security.Cryptography.Xml;
using FluentValidation;
using FluentValidation.Validators;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class EcropReportDocumentValidator : AbstractValidator<CropReportDocument>
	{
		private readonly List<string> m_SupportedTypeCodes = new List<string>() {"SCC", "SMN"};
		public EcropReportDocumentValidator()
		{
			RuleFor(x => x.TypeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField)
				.Must(typeCode => m_SupportedTypeCodes.Contains(typeCode))
				.WithState(x1 => MessageCode.CodeNotSupported);
			RuleFor(x => x.PurposeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.SenderSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
			RuleFor(x => x.RecipientSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
			RuleFor(x => x.IssueDateTime)
				.SetValidator(new DateTimeValidator());
		}
	}
}