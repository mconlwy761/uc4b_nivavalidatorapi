﻿using System.Linq;
using FluentValidation;
using FluentValidation.Validators;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class GrownFieldCropValidator : AbstractValidator<GrownFieldCrop>
	{
		/*
		* DK: 11 “Winter Wheat” (classification, PA crop)
    * This is the directorate crop, to be placed in the FieldCrop Classification field (Directorate is shorthand for the Danish PA)
		* In the field plan, this corresponds to 5923 “Winter Wheat” in the center normset 6504 SEGES English norms.
		* DK: 260 “Grass with clover/alfalfa with under 50 % legumes. (rotation)” (classification, PA crop)
		* This is the directorate crop to be put into the FieldCrop.Classification field. The name from the Danish Agricultural agency is used . In Mark Online (Danish FMIS) the name is ”260 O. græs m. kl/luc. u 50% bælg”.
		* On the field plan, this corresponds to ” 6086 Clover grass moving 31-50% cl.” in the center norm set 6504 SEGES English norms.
		* NL: 266 “Grasland, tijdelijk” (classification, PA crop)
		* We use the standard crop 6086 “Clover grass moving 31-50% cl.” from the English norm set. 
		* In the user norm set for Niva Projekt the standard crop own name is “266 Grasland, tijdelijk” 
		* NL: 265 “Grasland, blijvend” (classification, PA crop)
		* We use the standard crop 6127 “Perm. gr. M, <50% cl.”. from the English norm set. 
		* In the user norm set for Niva Projekt the standard crop own name is “265 Grasland, blijvend” 
		*/
		private static readonly string[] ValidClassificationCodesDK = new string[]{"11","260"};
		/*
		* NL:  233 “Wintertarve” (classification, PA crop)
		* We use the standard crop 5923 “Winter wheat” from the English norm set
		* In the user norm set for Niva Projekt the standard crop own name is “233 Wintertarve”
		* NL:  234 “Zomertarve” (classification, PA crop)
		* We use the standard crop 5918 “Spring wheat” from the English norm set
		* In the user norm set for Niva Projekt the standard crop own name is “234 Zomertarve”
		* NL: 266 “Grasland, tijdelijk” (classification, PA crop)
		* We use the standard crop 6086 “Clover grass moving 31-50% cl.” from the English norm set. 
		* In the user norm set for Niva Projekt the standard crop own name is “266 Grasland, tijdelijk” 
		* NL: 265 “Grasland, blijvend” (classification, PA crop)
		* We use the standard crop 6127 “Perm. gr. M, <50% cl.”. from the English norm set. 
		* In the user norm set for Niva Projekt the standard crop own name is “265 Grasland, blijvend” 
		 */
		private static readonly string[] ValidClassificationCodesNL = new string[] { "233", "234", "265", "266" };

		public GrownFieldCropValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.SpecifiedFieldCropMixtureConstituent)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.Must(x => reportTypeCode != ReportTypeCode.SCC || x.Length >= 2)
				.WithState(x1 => MessageCode.TwoOrMoreConstituents)
				.ForEach(mixture => mixture.SetValidator(new SpecifiedFieldCropMixtureConstituentValidator(organisationCode, reportTypeCode)));
			RuleFor(x => x.ApplicableCropProductionAgriculturalProcess)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(mixture => mixture.SetValidator(new ApplicableCropProductionAgriculturalProcessValidator(organisationCode, reportTypeCode)));
			if (reportTypeCode == ReportTypeCode.SMN)
			{
				if (organisationCode == OrganisationCode.DK)
				{
					RuleFor(x => x.ClassificationCode)
						.NotNull()
						.WithState(x1 => MessageCode.NullField)
						.Must(x2 => ValidClassificationCodesDK.Contains(x2))
						.WithState(x3 => MessageCode.ClassificationCodeNotSupported);
				}

				if (organisationCode == OrganisationCode.NL)
				{
					RuleFor(x => x.ClassificationCode)
						.NotNull()
						.WithState(x1 => MessageCode.NullField)
						.Must(x2 => ValidClassificationCodesNL.Contains(x2))
						.WithState(x3 => MessageCode.ClassificationCodeNotSupported);
				}
			}
		}
	}
}
