﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Validators;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class ApplicableCropProductionAgriculturalProcessValidator :AbstractValidator<ApplicableCropProductionAgriculturalProcess>
	{
		public ApplicableCropProductionAgriculturalProcessValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.ActualStartDateTime)
				.SetValidator(new DateTimeValidator())
				.SetValidator(new DateCanNotBeFutureValidator());
			RuleFor(x => x.AppliedSpecifiedAgriculturalApplication)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new AppliedSpecifiedAgriculturalApplicationValidator(organisationCode, reportTypeCode)));
		}
	}
}
