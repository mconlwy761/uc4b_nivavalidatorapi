﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedBotanicalCropValidator :AbstractValidator<SpecifiedBotanicalCrop>
	{
        public SpecifiedBotanicalCropValidator(OrganisationCode organisationCode)
		{
			RuleFor(x => x.BotanicalIdentificationId)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.Must(id => m_BotanicalIdentificationCodes.ContainsKey(organisationCode) == false || m_BotanicalIdentificationCodes[organisationCode].Contains(id))
				.WithState(x2 => MessageCode.OrganisationSpecificCodeNotSupported);
		}

        private readonly Dictionary<OrganisationCode, List<string>> m_BotanicalIdentificationCodes = new Dictionary<OrganisationCode, List<string>>()
        {
            /*
			 * 5707 Oil radish
			 * 5711 Græsmarksplant./Grass field species
			 * 5765 Egyptian clover
			 * 5799 Vårhvede/Spring wheat
			 * 5793 Vinterhvede/Winter wheat
			 * 5795 Alm.rajg., m.tidl, græsm./Perennial ryegrass, m.early, forage
			 * 1673138 Efterafgrøder/Catch crops
			 * 1674099 Phacelia
			 */
            { OrganisationCode.DK,new List<string>(){ "5707", "5711", "5765", "5799", "5793","5795","1673138", "1674099"}},
            { OrganisationCode.NL,new List<string>(){ "5707", "5711", "5765", "5799", "5793","5795","1673138", "1674099"}}
        };
    }
}
