﻿using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class AgriculturalProducerPartyValidator : AbstractValidator<AgriculturalProducerParty>
	{
		public AgriculturalProducerPartyValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.AgriculturalProductionUnit)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new AgriculturalProductionUnitValidator(organisationCode, reportTypeCode));
		}
	}
}
