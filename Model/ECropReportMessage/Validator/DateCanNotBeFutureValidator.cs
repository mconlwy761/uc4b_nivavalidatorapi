﻿using System;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class DateCanNotBeFutureValidator : AbstractValidator<string>
	{
		public DateCanNotBeFutureValidator()
		{
			RuleFor(x => x)
				.Must(x =>
				{
					if (DateTime.TryParse(x, out DateTime date))
						return date.Date <= DateTime.Today;
					return true;
				})
				.WithState(x => MessageCode.DateCanNotBeFuture);
		}
	}
}