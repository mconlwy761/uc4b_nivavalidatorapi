﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedCropPlotValidator : AbstractValidator<SpecifiedCropPlot>
	{
		public SpecifiedCropPlotValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.GrownFieldCrop)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(crop => crop.SetValidator(new GrownFieldCropValidator(organisationCode, reportTypeCode)));
			RuleFor(x => x.SpecifiedReferencedLocation)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new ReferencedLocationValidator());
			RuleFor(x => x.StartDateTime)
				.SetValidator(new DateTimeValidator());
			RuleFor(x => x.EndDateTime)
				.SetValidator(new DateTimeValidator());
		}
	}
}
