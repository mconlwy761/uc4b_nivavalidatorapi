﻿using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class ECropRequestValidator :AbstractValidator<ECropRequest>
	{
		public ECropRequestValidator()
		{
			RuleFor(x => x.EcropReportMessage)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new EcropReportMessageValidator());
		}
	}
}
