﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class AgriculturalProductionUnitValidator :AbstractValidator<AgriculturalProductionUnit>
	{
		public AgriculturalProductionUnitValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.SpecifiedCropPlot)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(plot => plot.SetValidator(new SpecifiedCropPlotValidator(organisationCode, reportTypeCode)));
		}
	}
}
