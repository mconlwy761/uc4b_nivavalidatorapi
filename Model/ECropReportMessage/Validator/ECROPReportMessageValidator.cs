﻿using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class EcropReportMessageValidator : AbstractValidator<EcropReportMessage>
	{
		public EcropReportMessageValidator()
		{
			RuleFor(x => x.CropReportDocument)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new EcropReportDocumentValidator());
			RuleFor(x => x.AgriculturalProducerParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(x =>
					new AgriculturalProducerPartyValidator(
						NivaUtil.GetOrganisationCode(x.CropReportDocument.RecipientSpecifiedParty.CountryId), NivaUtil.GetReportTypeCode(x.CropReportDocument.TypeCode)));
		}

	}
}