﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class ReferencedLocationValidator :AbstractValidator<ReferencedLocation>
	{
		public ReferencedLocationValidator()
		{
			RuleFor(x => x.PhysicalSpecifiedGeographicalFeature)
				.SetValidator(new PhysicalSpecifiedGeographicalFeatureValidator());
		}
	}
}
