﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// This is the specific production unit under an agricultural producer party
	/// </summary>
	public partial class AgriculturalProductionUnit
	{
		/// <summary>
		/// Identifier of this agricultural production unit.
		/// </summary>
		[JsonProperty("ID", Required = Required.Always)]
		public string Id { get; set; }

		/// <summary>
		/// The name, as text, of this agricultural production unit.
		/// </summary>
		[JsonProperty("Name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Name { get; set; }

		/// <summary>
		/// An array of Specified Crop Plots
		/// </summary>
		[JsonProperty("SpecifiedCropPlot", Required = Required.Always)]
		public SpecifiedCropPlot[] SpecifiedCropPlot { get; set; }

		/// <summary>
		/// The description, as text, of this agricultural production unit.
		/// </summary>
		[JsonProperty("TypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string TypeCode { get; set; }
	}
}