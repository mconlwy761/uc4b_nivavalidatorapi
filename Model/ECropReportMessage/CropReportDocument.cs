﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage
{
    /// <summary>
    /// A collection of data for a piece of written, printed or electronic matter that is
    /// exchanged between two or more parties for crop reporting purposes.
    /// </summary>
    public partial class CropReportDocument
    {
        /// <summary>
        /// Does this crop report document have specific control requirements.
        /// </summary>
        [JsonProperty("ControlRequirementIndicator", Required = Required.Always)]
        public bool ControlRequirementIndicator { get; set; }

        /// <summary>
        /// The indication of whether or not this crop report document is a copy.
        /// </summary>
        [JsonProperty("CopyIndicator", Required = Required.Always)]
        public bool CopyIndicator { get; set; }

        /// <summary>
        /// The textual description of this crop report document.
        /// </summary>
        [JsonProperty("Description", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>
        /// The identifier of this crop report document.
        /// </summary>
        [JsonProperty("ID", Required = Required.Always)]
        public string Id { get; set; }

        /// <summary>
        /// Information, as text, for this crop report document.
        /// </summary>
        [JsonProperty("Information", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Information { get; set; }

        /// <summary>
        /// The date, time, date time or other date time value for the issuance of this crop report
        /// document.
        /// </summary>
        [JsonProperty("IssueDateTime", Required = Required.Always)]
        public string IssueDateTime { get; set; }

        /// <summary>
        /// Number of lines in this crop report document.
        /// </summary>
        [JsonProperty("LineCountNumeric", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? LineCountNumeric { get; set; }

        /// <summary>
        /// Code specifying the purpose of this crop report document.
        /// </summary>
        [JsonProperty("PurposeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string PurposeCode { get; set; }

        [JsonProperty("RecipientSpecifiedParty", Required = Required.Always)]
        public SpecifiedParty RecipientSpecifiedParty { get; set; }

        /// <summary>
        /// The number of reports in this crop report document. Multiple reports – what will
        /// distinguish different reports?
        /// </summary>
        [JsonProperty("ReportCountNumeric", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public long? ReportCountNumeric { get; set; }

        [JsonProperty("SenderSpecifiedParty", Required = Required.Always)]
        public SpecifiedParty SenderSpecifiedParty { get; set; }

        /// <summary>
        /// Identifier specifying the sequence number of this crop report document.
        /// </summary>
        [JsonProperty("SequenceID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string SequenceId { get; set; }

        /// <summary>
        /// Code specifying the status of this crop report document.
        /// </summary>
        [JsonProperty("StatusCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string StatusCode { get; set; }

        /// <summary>
        /// The code specifying the type of crop report document.
        /// Supported <seealso cref="ReportTypeCode"/>
        /// </summary>
        [JsonProperty("TypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string TypeCode { get; set; }
    }
}