﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// An individual, a group, or a body having a role in the production of crops such as
	/// vegetables, flowers, plants.
	/// </summary>
	public partial class AgriculturalProducerParty
	{
		/// <summary>
		/// This is the specific production unit under an agricultural producer party
		/// </summary>
		[JsonProperty("AgriculturalProductionUnit", Required = Required.Always)]
		public AgriculturalProductionUnit AgriculturalProductionUnit { get; set; }

		/// <summary>
		/// A code specifying a classification for this agricultural producer party
		/// </summary>
		[JsonProperty("ClassificationCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string ClassificationCode { get; set; }

		/// <summary>
		/// The description, as text, of this agricultural producer party.
		/// </summary>
		[JsonProperty("Description", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Description { get; set; }

		/// <summary>
		/// Identifier of this agricultural producer party.
		/// </summary>
		[JsonProperty("ID", Required = Required.Always)]
		public string Id { get; set; }

		/// <summary>
		/// The name, as text, of this agricultural producer party.
		/// </summary>
		[JsonProperty("Name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Name { get; set; }
	}
}