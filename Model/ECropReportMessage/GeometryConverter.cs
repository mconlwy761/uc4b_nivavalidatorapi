﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NivaValidatorApi.Model.ECropReportMessage
{

	public class GeometryConverter : JsonConverter<Geometry>
	{
		public override void WriteJson(JsonWriter writer, Geometry value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value);
		}

		public override Geometry ReadJson(
			JsonReader reader,
			Type objectType,
			Geometry existingValue,
			bool hasExistingValue,
			JsonSerializer serializer)
		{
			JObject jObject = JObject.Load(reader);
			var geometryType = jObject["type"]?.Value<string>();
			if (string.IsNullOrWhiteSpace(geometryType))
			{
				throw new ArgumentNullException(nameof(geometryType));
			}
			var coordinates = jObject["coordinates"]?.ToString();
			if (string.IsNullOrWhiteSpace(coordinates))
			{
				throw new ArgumentNullException(nameof(coordinates));
			}

			switch (geometryType)
			{
				case "Polygon":
					return new PolygonGeometry
					{
						coordinates = JsonConvert.DeserializeObject<double[][][]>(coordinates)
					};

				case "Point":
					return new PointGeometry
					{
						coordinates = JsonConvert.DeserializeObject<double[]>(coordinates)
					};
			}

			throw new NotImplementedException();
		}
	}
}
