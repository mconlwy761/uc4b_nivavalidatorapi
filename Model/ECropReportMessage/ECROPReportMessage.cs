﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// Document envelope
	/// </summary>
	public partial class EcropReportMessage
	{
		/// <summary>
		/// A collection of data for a piece of written, printed or electronic matter that is
		/// exchanged between two or more parties for crop reporting purposes.
		/// </summary>
		[JsonProperty("CropReportDocument", Required = Required.Always)]
		public CropReportDocument CropReportDocument { get; set; }

		/// <summary>
		/// An individual, a group, or a body having a role in the production of crops such as
		/// vegetables, flowers, plants.
		/// </summary>
		[JsonProperty("AgriculturalProducerParty", Required = Required.Always)]
		public AgriculturalProducerParty AgriculturalProducerParty { get; set; }

	}
}