﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// The physical field, described as a polygon.
	/// </summary>
	public partial class ReferencedLocation
	{
		/// <summary>
		/// Textual description of this crop plot location.
		/// </summary>
		[JsonProperty("Description", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Description { get; set; }

		/// <summary>
		/// Id of this crop plot location.
		/// </summary>
		[JsonProperty("ID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Id { get; set; }

		/// <summary>
		/// mane of this crop plot location.
		/// </summary>
		[JsonProperty("Name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Name { get; set; }

		/// <summary>
		/// A GeoJSON object, specifying the crop plot location in lat-lon decimal degrees, GEoJSON
		/// uses the WGS84 datum.
		/// </summary>
		[JsonProperty("PhysicalSpecifiedGeographicalFeature", Required = Required.Always)]
		public PhysicalSpecifiedGeographicalFeature PhysicalSpecifiedGeographicalFeature { get; set; }

		/// <summary>
		/// Code describing the reference type for this referenced location.
		/// </summary>
		[JsonProperty("ReferenceTypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string ReferenceTypeCode { get; set; }

		/// <summary>
		/// Code for the type of crop plot location.
		/// </summary>
		[JsonProperty("TypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string TypeCode { get; set; }
	}
}