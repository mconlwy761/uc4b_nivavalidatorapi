﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// The sender of this crop report document.
	/// </summary>
	public partial class SpecifiedParty
	{
		/// <summary>
		/// The country identifier for this specified party.
		/// </summary>
		[JsonProperty("CountryID", Required = Required.Always)]
		public string CountryId { get; set; }

		/// <summary>
		/// Identifier of this specified party.
		/// </summary>
		[JsonProperty("ID", Required = Required.Always)]
		public string Id { get; set; }

		/// <summary>
		/// The name, as text, of this specified party.
		/// </summary>
		[JsonProperty("Name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Name { get; set; }
	}
}