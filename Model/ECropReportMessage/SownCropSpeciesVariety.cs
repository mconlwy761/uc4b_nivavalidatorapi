﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// A sub-division of cultivated plants within a species.
	/// </summary>
	public partial class SownCropSpeciesVariety
	{
		/// <summary>
		/// The textual description for this crop species variety.
		/// </summary>
		[JsonProperty("Description", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string Description { get; set; }

		/// <summary>
		/// The code specifying the type of crop species variety.
		/// </summary>
		[JsonProperty("TypeCode", Required = Required.Always)]
		public string TypeCode { get; set; }
	}
}