﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NetTopologySuite.Geometries;
using NetTopologySuite.CoordinateSystems.Transformations;
using Newtonsoft.Json;
using NivaValidatorApi.Model.ECropReportMessage;
using NivaValidatorApi.Model.Util;
using NivaValidatorApi.Model.Util.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json.Serialization;
using GeoAPI.Geometries;
using Geometry = NivaValidatorApi.Model.ECropReportMessage.Geometry;

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class EcropReceipt
	{
		private ReportTypeCode reportType;
		private OrganisationCode organisation;

		public EcropReceipt(EcropReportMessage reportMessage)
		{
			reportType = NivaUtil.GetReportTypeCode(reportMessage.CropReportDocument.TypeCode);
			organisation = NivaUtil.GetOrganisationCode(reportMessage.CropReportDocument.RecipientSpecifiedParty.CountryId);
			Map(reportMessage.CropReportDocument);
			Map(reportMessage.AgriculturalProducerParty);
		}
		#region properties
		
		/// <summary>
		/// Sender
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Sender")]
		public string Sender { get; set; }
		
		/// <summary>
		/// Recipient
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Recipient")]
		public string Recipient { get; set; }
		
		/// <summary>
		/// Purpose
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Purpose")]
		public string Purpose { get; set; }

		/// <summary>
		/// List of Crop Plot (Field)
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("CropPlot")]
		public List<ReceiptCropPlot> CropPlot { get; set; }

		#endregion

		#region mappers
		private void Map(CropReportDocument cropReportDocument)
		{
			Sender = cropReportDocument.SenderSpecifiedParty.Name;
			Recipient = cropReportDocument.RecipientSpecifiedParty.Name;
			Purpose = cropReportDocument.PurposeCode;
		}

		private void Map(AgriculturalProducerParty agriculturalProducerParty)
		{
			Map(agriculturalProducerParty.AgriculturalProductionUnit);
		}

		private void Map(AgriculturalProductionUnit agriculturalProductionUnit)
		{
			CropPlot = agriculturalProductionUnit.SpecifiedCropPlot
				.Select(Map).ToList();
		}

		private ReceiptCropPlot Map(SpecifiedCropPlot cropPlot)
		{
			int year=0;
			if (DateTime.TryParse(cropPlot.StartDateTime, out var start))
				year = start.Year + 1;
			var result = new ReceiptCropPlot()
			{
				FieldNumber = cropPlot.Id,
				Year = year
			};

			if (reportType == ReportTypeCode.SCC)
			{
				result.LegislationCrop = cropPlot.GrownFieldCrop.Select(x => x.ClassificationCode)
					.Distinct()
					.ToArray();
				result.CropConstituents = cropPlot.GrownFieldCrop.SelectMany(x =>
						x.SpecifiedFieldCropMixtureConstituent.Select(x => x.SpecifiedBotanicalCrop.BotanicalName))
					.Distinct()
					.ToArray();
				result.AppliedList = cropPlot.GrownFieldCrop.SelectMany(x => x.ApplicableCropProductionAgriculturalProcess.Select(Map))
					.ToArray();
			}

			if (reportType == ReportTypeCode.SMN)
			{
				result.LegislationCrop = cropPlot.GrownFieldCrop.Select(x => x.ClassificationCode)
					.Distinct()
					.ToArray();
				result.Produce = cropPlot.GrownFieldCrop.SelectMany(x =>
					x.ApplicableCropProductionAgriculturalProcess.SelectMany(x =>
						x.AppliedSpecifiedAgriculturalApplication.SelectMany(x =>
							x.SpecifiedProductBatch.Select(x => x.ProductName))))
					.Distinct()
					.ToArray();
				result.AppliedList = cropPlot.GrownFieldCrop.SelectMany(x => x.ApplicableCropProductionAgriculturalProcess.Select(Map))
					.ToArray();
			}
			return result;
		}
		private ReceiptApplied Map(ApplicableCropProductionAgriculturalProcess process)
		{
			var isPointData = process.AppliedSpecifiedAgriculturalApplication.FirstOrDefault()
				?.AppliedSpecifiedAgriculturalApplicationRate.FirstOrDefault()
				?.AppliedReferencedLocation.FirstOrDefault()
				?.PhysicalSpecifiedGeographicalFeature.Geometry.type == GeometryType.Point;

			double areaAppliedTotal;
			double quantityTotal;
			if (isPointData)
			{
				var firstElement = process.AppliedSpecifiedAgriculturalApplication.First();
				areaAppliedTotal = firstElement.TotalArea ?? 0;
				quantityTotal = firstElement.TotalProductAmount ?? 0;
			}
			else
			{
				(quantityTotal, areaAppliedTotal) = MapAreaAndQuantity(process);
			}

			IList<double> appliedQuantities = process.AppliedSpecifiedAgriculturalApplication.SelectMany(x =>
					x.AppliedSpecifiedAgriculturalApplicationRate.Select(x =>
						x.AppliedQuantity))
				.ToList();

			DateTime.TryParse(process.ActualStartDateTime, out var applicationDate);
			return new ReceiptApplied()
			{
				AreaAppliedTotal = areaAppliedTotal,
				// TODO: Take Applied Quantity unit into account. Amount is per area unit (sample message says /ha), som this must be taken into account.
				AverageApplicationRate = quantityTotal / (areaAppliedTotal / 10000), //We have hectares in appliedQuantities, and area is square meters.
				ApplicationDate = applicationDate,
				MaxApplicationRate =appliedQuantities.Max(),
				MinApplicationRate = appliedQuantities.Min(),
			};
		}

		private (double Quantity, double Area) MapAreaAndQuantity(ApplicableCropProductionAgriculturalProcess process)
		{
			IEnumerable<double> areas = process.AppliedSpecifiedAgriculturalApplication.SelectMany(x =>
				x.AppliedSpecifiedAgriculturalApplicationRate.SelectMany(x =>
					x.AppliedReferencedLocation.Select(x =>
						CalculateArea(x.PhysicalSpecifiedGeographicalFeature.Geometry))));
			var areaAppliedTotal = areas.Sum();

			IEnumerable<AppliedSpecifiedAgriculturalApplicationRate> cells = process.AppliedSpecifiedAgriculturalApplication.SelectMany(x =>
				x.AppliedSpecifiedAgriculturalApplicationRate);

			double quantityTotal = 0;

			foreach (AppliedSpecifiedAgriculturalApplicationRate cell in cells)
			{
				double quantityPerHa = cell.AppliedQuantity;
				double areaHa = cell.AppliedReferencedLocation.Sum(x => CalculateArea(x.PhysicalSpecifiedGeographicalFeature.Geometry)) / 10000;
				quantityTotal += quantityPerHa * areaHa;
			}

			return (quantityTotal, areaAppliedTotal);
		}

		public double CalculateArea(Geometry geometry)
		{
			if (geometry is PolygonGeometry polygonGeometry)
			{
				// Do this very much up front in order to get a longitude, so that we can determine the UTM zone
				var allLinearRings = polygonGeometry.coordinates
					.Select(linearRings => linearRings.Select(p => new Coordinate(p[0], p[1])).ToArray())
					.ToArray();

				var outerLinearRing = new LinearRing(allLinearRings[0]);
				var innerLinearRings = allLinearRings.Skip(1).Select(t => new LinearRing(t)).ToArray<ILinearRing>();

				var polygon = LatLongToUTM(new Polygon(outerLinearRing, innerLinearRings, new GeometryFactory(new PrecisionModel(), 4326)));

				return polygon?.Area ?? 0;
			}

			return 0;
		}

		public static IGeometry LatLongToUTM(IGeometry geometry)
		{
			if (geometry.IsEmpty)
				return null;
			var utmZoneNo = Convert.ToInt32(Math.Round((geometry.Centroid.X + 180)/6));
			var isNorth = geometry.Centroid.Y >= 0;

			var ctFact = new ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory();
			var wgs84 = ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84;
			var utmZone = ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WGS84_UTM(utmZoneNo, isNorth);
			var transformation = ctFact.CreateFromCoordinateSystems(wgs84, utmZone);
			IGeometry transformedGeometry = GeometryTransform.TransformGeometry(GeometryFactory.Default, geometry, transformation.MathTransform);

			return transformedGeometry;
		}

		#endregion

	}
}
