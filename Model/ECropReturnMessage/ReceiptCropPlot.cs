﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class ReceiptCropPlot
	{
		/// <summary>
		/// 
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Year")]
		public int Year { get; set; }

		/// <summary>
		/// FieldNumber
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("FieldNumber")]
		public string FieldNumber { get; set; }

		/// <summary>
		/// LegislationCrop
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("LegislationCrop")]
		public string[] LegislationCrop { get; set; }

		/// <summary>
		/// CropConstituents
		/// SCC
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("CropConstituents")]
		public string[] CropConstituents { get; set; }

		/// <summary>
		/// AppliedList
		/// SCC
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("AppliedList")]
		public ReceiptApplied[] AppliedList { get; set; }

		/// <summary>
		/// Produce
		/// SMN
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Produce")]
		public string[] Produce { get; set; }
	}
}
