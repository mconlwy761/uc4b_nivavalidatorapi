﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;


namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class ECROPReturnMessage
	{
		/// <summary>
		/// 0 = No errors, 2 = Info Messages present, 5 = Warnings, 9 = errors encountered
		/// </summary>
		[JsonProperty]
//		[JsonPropertyName("ReturnCode")]
		public int ReturnCode => (int) ReturnCodeEnum;

		[System.Text.Json.Serialization.JsonIgnore]
		public ReturnCodeEnum ReturnCodeEnum { get; set; }

		/// <summary>
		/// Either 'Received Ok', 'Validated Ok' (for ReturnCode 0) or 'Warnings' or 'Errors encountered'
		/// </summary>
		[JsonProperty]
//		[JsonPropertyName("ReturnMessage")]
		public string ReturnMessage
		{
			get
			{
				switch (ReturnCodeEnum)
				{
					case ReturnCodeEnum.No_errors:
						return "No errors";
					case ReturnCodeEnum.Info_Messages_present:
						return "Info Messages present";
					case ReturnCodeEnum.Warnings:
						return "Warnings";
					case ReturnCodeEnum.Errors_encountered:
						return "Errors encountered";
					default:
						throw new NotImplementedException(ReturnCode.ToString());
				}
			}
		}

		/// <summary>
		/// Number of messages found, should be 0 in an OK message
		/// </summary>
		[JsonProperty]
//		[JsonPropertyName("MessageCount")]
		public int MessageCount { get; set; }
		/// <summary>
		/// Timestamp
		/// </summary>
		[JsonProperty]
//		[JsonPropertyName("TimeStamp")]
		public DateTime TimeStamp { get; set; }
		
		/// <summary>
		/// Messages
		/// </summary>
		[JsonProperty]
//		[JsonPropertyName("Messages")]
		public List<MessageDto> Messages { get; set; }

	}
}
