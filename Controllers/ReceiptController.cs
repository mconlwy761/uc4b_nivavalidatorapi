﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using NivaValidatorApi.Model.ECropReportMessage;
using NivaValidatorApi.Model.ECropReportMessage.Validator;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ReceiptController: ControllerBase
	{
		/// <summary>
		/// Validate with receipt.
		/// Returns null if validate fails
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[ProducesResponseType(typeof(EcropReceipt), 200)]
		[ProducesResponseType(typeof(void), 204)]
		[HttpPost]
		public async Task<IActionResult> Validate([FromBody] ECropRequest request)
		{
			var ecropReportMessageDtoValidator = new ECropRequestValidator();
			ValidationResult validationResult = await ecropReportMessageDtoValidator.ValidateAsync(request);

			EcropReceipt result = validationResult.IsValid ? new EcropReceipt(request.EcropReportMessage) : null;
			
			return Ok(result);
		}
	}
}