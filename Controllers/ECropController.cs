﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using NivaValidatorApi.Model.ECropReportMessage;
using NivaValidatorApi.Model.ECropReportMessage.Validator;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ECropController : ControllerBase
	{
		[ProducesResponseType(typeof(ECropResponse), 200)]
		[HttpPost]
		public async Task<IActionResult> Validate([FromBody] ECropRequest request)
		{
			var ecropReportMessageDtoValidator = new ECropRequestValidator();
			ValidationResult validationResult = await ecropReportMessageDtoValidator.ValidateAsync(request);

			var result = new ECropResponse()
			{
				ECROPReturnMessage = new ECROPReturnMessage()
				{
					ReturnCodeEnum = validationResult.IsValid ? ReturnCodeEnum.No_errors : ReturnCodeEnum.Errors_encountered,
					MessageCount = validationResult.Errors.Count,
					Messages = !validationResult.IsValid ?
						validationResult.Errors.Select(x => new MessageDto(x.PropertyName, (MessageCode)(x.CustomState ?? MessageCode.None), x.Severity.ToString()))
							.ToList() : null,
					TimeStamp = DateTime.Now
				}
			}; 
			
			return Ok(result);
		}
	}
}