/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Serilog;

namespace NivaValidatorApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
	        string[] AllowedCorsUrls = Configuration.GetSection("AllowedCorsUrls").Get<string[]>();
	        services.AddCors(options =>
	        {
		        options.AddPolicy("AllowOriginsFromAppSettingsJson",
			        builder => builder.WithOrigins(AllowedCorsUrls).AllowAnyHeader());
	        });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = "NivaValidatorApi", 
                    Version = "v1",
                    Description = "Api for validating machine data",
                    Contact = new OpenApiContact()
										{
											Name = "Troels Agerbo",
											Email = "tag@seges.dk",
											Url = new Uri("https://mit.intra.lf.dk/Person.aspx?accountname=LC%5CTAG")
										},
                    License = new OpenApiLicense()
                    {
	                    Name = "license",
	                    Url = new Uri("https://seges.dk/license")
                    }
								});
              c.SchemaFilter<MySchemaFilter>();
              
              // Set the comments path for the Swagger JSON and UI.
              var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
              var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
              c.IncludeXmlComments(xmlPath);

            });

			services.AddMvc()
	            .AddJsonOptions(options =>
					{
						options.JsonSerializerOptions.PropertyNamingPolicy = null;
						options.JsonSerializerOptions.IgnoreNullValues = true;
						options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
					}
				);
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Anvend Serilog RequestLogging
            app.UseSerilogRequestLogging();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            string nivaValidatorApiBaseUrl = Configuration["ApplicationSettings:NivaValidatorApiBaseUrl"];

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{nivaValidatorApiBaseUrl}/swagger/v1/swagger.json", "NivaValidatorApi V1");
            });
        }
    }
}
